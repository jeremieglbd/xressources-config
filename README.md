# Using .Xressources file

The .Xressources file must be put in the user home directory.
It may be symbolicaly linked with ```ln```.

```
$ ln .Xressources ~/
$ xrdb ~/.Xressources
```
Use the [github repository](https://github.com/LukeSmithxyz) made by Luke Smith.
It will allow st (simple terminal) to use the .Xressources file.

``` bash
$ git clone https://gitlab.com/LukeSmithxyz/st.git
$ cd st
$ sudo make install
```

This last command needs ```make```.
In Fedora 31, the following packages are required :

* libX11-devel
* libXt-devel
* libXft-devel

``` bash
$ sudo dnf install libX11-devel libXt-devel libXft-devel
```
